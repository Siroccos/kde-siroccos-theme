#include <KConfig>
#include <KConfigGroup>

int main()
{
    KConfig globals("kdeglobals");
    KConfigGroup general(&globals, "General");
    if (general.readEntry(QStringLiteral("ColorScheme")) != QStringLiteral("Sirocco")) {
        return 0;
    }
    general.writeEntry(QStringLiteral("ColorScheme"), QStringLiteral("SiroccoClassic"));
    // No need to migrate the serialized colors because they're the same
}
