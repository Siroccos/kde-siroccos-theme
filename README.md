Sirocco est un fork de [Breeze](https://invent.kde.org/plasma/breeze) (basé sur le commit cc1356decc01c64ba1dbe8eee1f62467e0f7d271) dont l'objectif est de l'adapter pour correspondre à mes besoins.

Il est également fortement inspiré des projets suivants [Lightly](https://github.com/Luwx/Lightly) et [Nordic](https://github.com/EliverLara/Nordic/tree/master/kde).

