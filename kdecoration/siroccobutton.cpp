/*
 * SPDX-FileCopyrightText: 2014 Martin Gräßlin <mgraesslin@kde.org>
 * SPDX-FileCopyrightText: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */
#include "siroccobutton.h"

#include <KDecoration2/DecoratedClient>
#include <KColorUtils>
#include <KIconLoader>

#include <QPainter>
#include <QVariantAnimation>
#include <QPainterPath>

namespace Sirocco
{

    using KDecoration2::ColorRole;
    using KDecoration2::ColorGroup;
    using KDecoration2::DecorationButtonType;


    //__________________________________________________________________
    Button::Button(DecorationButtonType type, Decoration* decoration, QObject* parent)
        : DecorationButton(type, decoration, parent)
        , m_animation( new QVariantAnimation( this ) )
    {

        // setup animation
        // It is important start and end value are of the same type, hence 0.0 and not just 0
        m_animation->setStartValue( 0.0 );
        m_animation->setEndValue( 1.0 );
        m_animation->setEasingCurve( QEasingCurve::InOutQuad );
        connect(m_animation, &QVariantAnimation::valueChanged, this, [this](const QVariant &value) {
            setOpacity(value.toReal());
        });

        // setup default geometry
        const int height = decoration->buttonHeight();
        setGeometry(QRect(0, 0, height, height));
        setIconSize(QSize( height, height ));

        // connections
        connect(decoration->client().toStrongRef().data(), SIGNAL(iconChanged(QIcon)), this, SLOT(update()));
        connect(decoration->settings().data(), &KDecoration2::DecorationSettings::reconfigured, this, &Button::reconfigure);
        connect( this, &KDecoration2::DecorationButton::hoveredChanged, this, &Button::updateAnimationState );

        reconfigure();

    }

    //__________________________________________________________________
    Button::Button(QObject *parent, const QVariantList &args)
        : Button(args.at(0).value<DecorationButtonType>(), args.at(1).value<Decoration*>(), parent)
    {
        m_flag = FlagStandalone;
        //! icon size must return to !valid because it was altered from the default constructor,
        //! in Standalone mode the button is not using the decoration metrics but its geometry
        m_iconSize = QSize(-1, -1);
    }
            
    //__________________________________________________________________
    Button *Button::create(DecorationButtonType type, KDecoration2::Decoration *decoration, QObject *parent)
    {
        if (auto d = qobject_cast<Decoration*>(decoration))
        {
            Button *b = new Button(type, d, parent);
            const auto c = d->client().toStrongRef();
            switch( type )
            {

                case DecorationButtonType::Close:
                b->setVisible( c->isCloseable() );
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::closeableChanged, b, &Sirocco::Button::setVisible );
                break;

                case DecorationButtonType::Maximize:
                b->setVisible( c->isMaximizeable() );
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::maximizeableChanged, b, &Sirocco::Button::setVisible );
                break;

                case DecorationButtonType::Minimize:
                b->setVisible( c->isMinimizeable() );
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::minimizeableChanged, b, &Sirocco::Button::setVisible );
                break;

                case DecorationButtonType::ContextHelp:
                b->setVisible( c->providesContextHelp() );
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::providesContextHelpChanged, b, &Sirocco::Button::setVisible );
                break;

                case DecorationButtonType::Shade:
                b->setVisible( c->isShadeable() );
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::shadeableChanged, b, &Sirocco::Button::setVisible );
                break;

                case DecorationButtonType::Menu:
                QObject::connect(c.data(), &KDecoration2::DecoratedClient::iconChanged, b, [b]() { b->update(); });
                break;

                default: break;

            }

            return b;
        }

        return nullptr;

    }

    //__________________________________________________________________
    void Button::paint(QPainter *painter, const QRect &repaintRegion)
    {
        Q_UNUSED(repaintRegion)

        if (!decoration()) return;

        painter->save();

        // translate from offset
        if( m_flag == FlagFirstInList ) painter->translate( m_offset );
        else painter->translate( 0, m_offset.y() );

        if( !m_iconSize.isValid() || isStandAlone() ) m_iconSize = geometry().size().toSize();

        // menu button
        if (type() == DecorationButtonType::Menu)
        {

            const QRectF iconRect( geometry().topLeft(), m_iconSize );
            const auto c = decoration()->client().toStrongRef();
            if (auto deco =  qobject_cast<Decoration*>(decoration())) {
                const QPalette activePalette = KIconLoader::global()->customPalette();
                QPalette palette = c->palette();
                palette.setColor(QPalette::WindowText, deco->fontColor());
                KIconLoader::global()->setCustomPalette(palette);
                c->icon().paint(painter, iconRect.toRect());
                if (activePalette == QPalette()) {
                    KIconLoader::global()->resetPalette();
                }    else {
                    KIconLoader::global()->setCustomPalette(palette);
                }
            } else {
                c->icon().paint(painter, iconRect.toRect());
            }

        } else {

            drawIcon( painter );

        }

        painter->restore();

    }

    //__________________________________________________________________
    void Button::drawIcon( QPainter *painter ) const
    {

        painter->setRenderHints( QPainter::Antialiasing );

        /*
        scale painter so that its window matches QRect( -1, -1, 20, 20 )
        this makes all further rendering and scaling simpler
        all further rendering is preformed inside QRect( 0, 0, 18, 18 )
        */
        painter->translate( geometry().topLeft() );

        const qreal width( m_iconSize.width() );
        painter->scale( width/20, width/20 );
        painter->translate( 1, 1 );

        // render background
        const QColor backgroundColor( this->backgroundColor() );
        if( backgroundColor.isValid() )
        {
            const QColor borderColor( 35, 40, 49 );
            QPen pen( borderColor );
            pen.setWidthF( PenWidth::Symbol*qMax((qreal)1.5, 20/width ) );

            painter->setPen( pen );
            painter->setBrush( backgroundColor );
            painter->drawEllipse( QRectF( 2, 2, 14, 14 ) );
        }

        // render mark
        const QColor foregroundColor( this->foregroundColor() );
        if( foregroundColor.isValid() )
        {

            // setup painter
            QPen pen( foregroundColor );
            pen.setCapStyle( Qt::RoundCap );
            pen.setJoinStyle( Qt::MiterJoin );
            pen.setWidthF( PenWidth::Symbol*qMax((qreal)1.0, 20/width ) );

            painter->setPen( pen );
            painter->setBrush( Qt::NoBrush );

            switch( type() )
            {

                case DecorationButtonType::Close:
                {
                    painter->drawLine( QPointF( 6, 6 ), QPointF( 12, 12 ) );
                    painter->drawLine( QPointF( 12, 6 ), QPointF( 6, 12 ) );
                    break;
                }

                case DecorationButtonType::Maximize:
                {
                    painter->setPen( Qt::NoPen );
                    painter->setBrush( foregroundColor );

                    painter->drawPolygon( QVector<QPointF>{
                                           QPointF( 7, 5 ),
                                           QPointF( 12, 5 ),
                                           QPointF( 13, 6 ),
                                           QPointF( 13, 11 )});
                    painter->drawPolygon( QVector<QPointF>{
                                           QPointF( 5, 7 ),
                                           QPointF( 5, 12 ),
                                           QPointF( 6, 13 ),
                                           QPointF( 11, 13 )});
                    break;
                }

                case DecorationButtonType::Minimize:
                {
                painter->drawLine( QPointF( 5, 9 ), QPointF( 13, 9 ) );
                    break;
                }

                case DecorationButtonType::OnAllDesktops:
                {
                    painter->setPen( Qt::NoPen );
                    painter->setBrush( foregroundColor );

                    painter->drawEllipse( QRectF( 6, 6, 6, 6 ) );
                    break;
                }

                case DecorationButtonType::Shade:
                {

                    if (isChecked())
                    {

                        painter->drawLine( QPointF( 4, 5.5 ), QPointF( 14, 5.5 ) );
                        painter->drawPolyline( QVector<QPointF> {
                            QPointF( 4, 8 ),
                            QPointF( 9, 13 ),
                            QPointF( 14, 8 )} );

                    } else {

                        painter->drawLine( QPointF( 4, 5.5 ), QPointF( 14, 5.5 ) );
                        painter->drawPolyline(  QVector<QPointF> {
                            QPointF( 4, 13 ),
                            QPointF( 9, 8 ),
                            QPointF( 14, 13 ) });
                    }

                    break;

                }

                case DecorationButtonType::KeepBelow:
                {

                    painter->drawPolyline(  QVector<QPointF> {
                        QPointF( 4, 5 ),
                        QPointF( 9, 10 ),
                        QPointF( 14, 5 ) });

                    painter->drawPolyline(  QVector<QPointF> {
                        QPointF( 4, 9 ),
                        QPointF( 9, 14 ),
                        QPointF( 14, 9 ) });
                    break;

                }

                case DecorationButtonType::KeepAbove:
                {
                    painter->drawPolyline(  QVector<QPointF> {
                        QPointF( 4, 9 ),
                        QPointF( 9, 4 ),
                        QPointF( 14, 9 ) });

                    painter->drawPolyline(  QVector<QPointF> {
                        QPointF( 4, 13 ),
                        QPointF( 9, 8 ),
                        QPointF( 14, 13 ) });
                    break;
                }


                case DecorationButtonType::ApplicationMenu:
                {
                    painter->drawRect( QRectF( 3.5, 4.5, 11, 1 ) );
                    painter->drawRect( QRectF( 3.5, 8.5, 11, 1 ) );
                    painter->drawRect( QRectF( 3.5, 12.5, 11, 1 ) );
                    break;
                }

                case DecorationButtonType::ContextHelp:
                {
                    QPainterPath path;
                    path.moveTo( 5, 6 );
                    path.arcTo( QRectF( 5, 3.5, 8, 5 ), 180, -180 );
                    path.cubicTo( QPointF(12.5, 9.5), QPointF( 9, 7.5 ), QPointF( 9, 11.5 ) );
                    painter->drawPath( path );

                    painter->drawRect( QRectF( 9, 15, 0.5, 0.5 ) );

                    break;
                }

                default: break;

            }

        }

    }

    //__________________________________________________________________
    QColor Button::foregroundColor() const
    {
        auto d = qobject_cast<Decoration*>( decoration() );
        if( !d )
            return QColor();

        auto c = d->client().toStrongRef();

        if( isHovered() ) {
            return QColor( 35, 40, 49 );
        } else if( type() == DecorationButtonType::OnAllDesktops && c->isOnAllDesktops() &&
                   isChecked() )
            return QColor( 50, 74, 125 );

        return QColor();
    }

    //__________________________________________________________________
    QColor Button::backgroundColor() const
    {
        auto d = qobject_cast<Decoration*>( decoration() );
        if( !d ) {

            return QColor();

        }

        auto c = d->client().toStrongRef();

        // Tricolor button
        const QColor closeColor( 191, 97, 106 );
        const QColor maximizeColor( 163, 190, 140 );
        const QColor minimizeColor( 235, 203, 139 );

        const QColor allDesktopsColor( 94, 129, 172 );

        const QColor defaultColor( d->titleBarColor() );

        if( isHovered() || c->isActive() ) {
            if( type() == DecorationButtonType::Close)
                return closeColor;
            else if( type() == DecorationButtonType::Maximize)
                return maximizeColor;
            else if( type() == DecorationButtonType::Minimize)
                return minimizeColor;
            else if( type() == DecorationButtonType::OnAllDesktops)
                return allDesktopsColor;
        }

        return defaultColor;
    }

    //________________________________________________________________
    void Button::reconfigure()
    {

        // animation
        auto d = qobject_cast<Decoration*>(decoration());
        if( d )  m_animation->setDuration( d->animationsDuration() );

    }

    //__________________________________________________________________
    void Button::updateAnimationState( bool hovered )
    {

        auto d = qobject_cast<Decoration*>(decoration());
        if( !(d && d->animationsDuration() > 0 ) ) return;

        m_animation->setDirection( hovered ? QAbstractAnimation::Forward : QAbstractAnimation::Backward );
        if( m_animation->state() != QAbstractAnimation::Running ) m_animation->start();

    }

} // namespace
