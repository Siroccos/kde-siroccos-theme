/*
 * SPDX-FileCopyrightText: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "siroccostyleplugin.h"
#include "siroccostyle.h"

#include <QApplication>

namespace Sirocco
{

    //_________________________________________________
    QStyle* StylePlugin::create( const QString &key )
    {
        if( key.toLower() == QStringLiteral( "sirocco" ) )
        {
            return new Style;
        }
        return nullptr;
    }

    //_________________________________________________
    QStringList StylePlugin::keys() const
    { return QStringList( QStringLiteral( "Sirocco" ) ); }

}
