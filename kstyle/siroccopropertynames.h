/*
 * SPDX-FileCopyrightText: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef siroccopropertynames_h
#define siroccopropertynames_h

namespace Sirocco
{

    struct PropertyNames
    {
        static const char noAnimations[];
        static const char noWindowGrab[];
        static const char netWMForceShadow[];
        static const char netWMSkipShadow[];
        static const char sidePanelView[];
        static const char toolButtonAlignment[];
        static const char menuTitle[];
        static const char alteredBackground[];
        static const char highlightNeutral[];
        static const char noSeparator[];
        static const char isTopMenu[];
        static const char bordersSides[];
    };

}

#endif
