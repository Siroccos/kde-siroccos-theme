#include <KColorScheme>
#include <KConfig>
#include <KConfigGroup>

#include <QDebug>

int main()
{
    KConfig globals("kdeglobals");
    KConfigGroup general(&globals, "General");
    if (general.readEntry("ColorScheme") != QLatin1String("SiroccoHighContrast")) {
        return 0;
    }
    QString siroccoDarkPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("color-schemes/SiroccoDark.colors"));
    if (siroccoDarkPath.isEmpty()) {
        return 0;
    }
    KConfig siroccoDark(siroccoDarkPath, KConfig::SimpleConfig);
    for (const auto &group : siroccoDark.groupList()) {
        auto destination = KConfigGroup(&globals, group);
        KConfigGroup(&siroccoDark, group).copyTo(&destination, KConfig::Notify);
    }
}
