/*
 * SPDX-FileCopyrightText: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "siroccopropertynames.h"

namespace Sirocco
{

    const char PropertyNames::noAnimations[] = "_kde_no_animations";
    const char PropertyNames::noWindowGrab[] = "_kde_no_window_grab";
    const char PropertyNames::netWMForceShadow[] = "_KDE_NET_WM_FORCE_SHADOW";
    const char PropertyNames::netWMSkipShadow[] = "_KDE_NET_WM_SKIP_SHADOW";
    const char PropertyNames::sidePanelView[] = "_kde_side_panel_view";
    const char PropertyNames::toolButtonAlignment[] = "_kde_toolButton_alignment";
    const char PropertyNames::menuTitle[] = "_sirocco_toolButton_menutitle";
    const char PropertyNames::alteredBackground[] = "_sirocco_altered_background";
    const char PropertyNames::highlightNeutral[] = "_kde_highlight_neutral";
    const char PropertyNames::noSeparator[] = "_sirocco_no_separator";
    const char PropertyNames::isTopMenu[] = "_sirocco_menu_is_top";
    const char PropertyNames::bordersSides[] = "_sirocco_borders_sides";

}
