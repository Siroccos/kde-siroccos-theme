#include <KColorScheme>
#include <KConfig>
#include <KConfigGroup>

#include <QDebug>

int main()
{
    KConfig globals("kdeglobals");
    KConfigGroup general(&globals, "General");
    if (general.readEntry("ColorScheme") != QLatin1String("Sirocco")) {
        return 0;
    }
    QString siroccoLightPath = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("color-schemes/SiroccoLight.colors"));
    if (siroccoLightPath.isEmpty()) {
        return 0;
    }
    KConfig siroccoLight(siroccoLightPath, KConfig::SimpleConfig);
    for (const auto &group : siroccoLight.groupList()) {
        auto destination = KConfigGroup(&globals, group);
        KConfigGroup(&siroccoLight, group).copyTo(&destination, KConfig::Notify);
    }
}
